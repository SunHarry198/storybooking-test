
const withPugins = require('next-compose-plugins')
const withImages = require('next-images')
require('dotenv').config()
const webpack = require('webpack')

const path = require('path')

const scssConfig = module.exports = {
  sassOptions: {
    includePaths: [path.join(__dirname, 'styles')],
  },
}

const nextConfig = {
  inlineImageLimit: 16384,
  images: {
    es: {
      minimumCacheTTL: 60
    },
    imageSizes: [16, 32, 48, 64, 96, 128, 256, 384],
    deviceSizes: [640, 750, 828, 1080, 1200, 1920, 2048, 3840],
    domains: [
      'res.cloudinary.com',
      'api.huyi.host',
      'lh3.googleusercontent.com',
      'avatars.githubusercontent.com',
      'graph.facebook.com'
    ],
    formats: ['image/avif', 'image/webp'],
    disableStaticImages: true
  },

  ...scssConfig,

  webpack(config) {
    config.plugins.push(new webpack.EnvironmentPlugin(process.env))

    return config
  }
}
const plugins = [withImages]
module.exports = withPugins(plugins, nextConfig)
