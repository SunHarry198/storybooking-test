import React, { useEffect } from 'react'
import dynamic from 'next/dynamic'
import { Page } from '@type/page'
import { useRouter } from 'next/router'
import BookingBillInfo from 'src/page-components/Booking'
import { useDispatch } from 'react-redux'
import actionStore from '@actionStore'
import { useAppSelector } from '@lib/helper/hook'
const HomeLayout = dynamic(() => import('@templates/Home'))
interface BookingBillInfoPageProps {
  data: any
}
const BookingBillInfoPage: Page<BookingBillInfoPageProps> = () => {
  const router = useRouter()
  const { id } = router.query
  const dispatch = useDispatch()
  useEffect(() => {
    if (id) {
      dispatch(
        actionStore.requestBookingInfo({
          smsCode: id
        })
      )
    }
  }, [dispatch, id])
  const totalData: any = useAppSelector((state) => state?.totalData)
  if (!totalData?.bookingInfo) {
    return <div>Không có phiếu khám phù hợp</div>
  }
  const bookingInfo = totalData?.bookingInfo
  return <BookingBillInfo bookingInfo={bookingInfo} />
}
BookingBillInfoPage.layout = HomeLayout

export default BookingBillInfoPage
