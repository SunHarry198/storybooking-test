import { urlPage } from '@init/init'
import moment from 'moment'
import { GetServerSideProps } from 'next'

const getSitemap = ({ host }: any) => {
  const getRouter = Object.keys(urlPage)
    .map((item) => {
      return `<url>
                 <loc> ${`${host}${(urlPage as any)[item]}`}</loc>
                  <lastmod>${moment().format('l, HH:mm')}</lastmod>
                    <changefreq>daily</changefreq>
                    <priority>0.9</priority>
              </url>`
    })
    .join('')

  return `<?xml version="1.0" encoding="utf-8"?>
        <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
        ${getRouter}
      </urlset>`
}

const Sitemap = () => {
  return <> Sitemap </>
}

export default Sitemap

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const { req, res } = ctx
  const host = req.rawHeaders[1]
  res.setHeader('Content-Type', 'text/xml')
  res.write(getSitemap({ host }))
  res.end()
  return {
    props: {}
  }
}
