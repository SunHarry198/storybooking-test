import Container from '@components/atoms/Container'
import { onPush } from '@lib/utils/func'
import { Result } from 'antd'
import { urlPage } from '@init/init'
import { useRouter } from 'next/router'
import Button from '@components/atoms/Button'

function Error({ statusCode }: any) {
  const router = useRouter()

  return (
    <Container>
      <Result
        status={404}
        title={statusCode}
        subTitle={
          <p>
            <h2>Xin lỗi vì sự cố này!</h2>
            <p>Chúng tôi sẽ khắc phục sơm nhất !</p>
          </p>
        }
        extra={
          <Button type='primary' onClick={onPush(urlPage.TrangChu, router)}>
            Back Home
          </Button>
        }
      />
    </Container>
  )
}

Error.getInitialProps = ({ res, err }: any) => {
  const statusCode = res ? res.statusCode : err ? err.statusCode : 404
  return { statusCode }
}

export default Error
