import ResultCustom from '@components/organisms/ResultCustom'
import React from 'react'

const NotFound = () => {
  return (
    <>
      <ResultCustom
        subTitle={<p>Chúng tôi không tìm thấy thông tin bạn cần tìm !</p>}
      />
    </>
  )
}

export default NotFound
