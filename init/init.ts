export const urlPage = {
  DangNhap: '/dang-nhap',
  TrangChu: '/',
  ChamSocKhachHang: '/cham-soc-khach-hang',
  AuthVerifyCskh: '/auth-verify-cskh',
  TaoHoSo: '/tao-ho-so',
  BenhVien: '/benh-vien',
  Booking: 'booking'
}

export const urlTotal = {
  Medpro: 'https://medpro.vn'
}

export const urlLogin = {
  DEVELOP: 'https://account.trdthien.tech',
  PRODUCTION: 'https://account.trdthien.tech',
  // TESTING: 'https://id-testing.medpro.com.vn/check-phone',
  TESTING: 'http://localhost:3021',
  BETA: 'https://id-beta.medpro.com.vn/check-phone',
  LIVE: 'https://id-v121.medpro.com.vn/check-phone',
  HOTFIX: 'https://id-v121.medpro.com.vn/check-phone'
  // LOGIN: ''
}

export const urlMCO = {
  DEVELOP: 'https://api.huyi.host',
  TESTING: 'https://api.huyi.host',
  PRODUCTION: 'https://api.huyi.host'
}

export enum NameEnv {
  DEVELOP = 'develop',
  TESTING = 'testing',
  PRODUCTION = 'production'
}

export const urlBE = {
  DEVELOP: 'http://192.168.0.60:6510',
  TESTING: 'https://medpro-api-v3-testing.medpro.com.vn',
  PRODUCTION: 'https://medpro-api-v3-testing.medpro.com.vn',
  BETA: 'https://api-111.medpro.com.vn:5000',
  LIVE: 'https://api-120.medpro.com.vn:5000',
  HOTFIX: 'https://api-hotfix.medpro.com.vn:5000'
}

export const urlSupportCSKH = {
  DEVELOP: 'http://localhost:3013',
  TESTING: 'https://testing-v3.medpro.com.vn',
  BETA: 'https://umcmono-beta.medpro.com.vn',
  PRODUCTION: 'https://umcmono.medpro.vn'
}
