import { loadState } from '@lib/helper/localstorage'
import { AppState } from '@store/interface/appState'
import { rootReducer } from '@store/rootReducer'
import rootSaga from '@store/rootSaga'
import { createWrapper, MakeStore } from 'next-redux-wrapper'
import { createStore, Store } from 'redux'
import { Task } from 'redux-saga'
import { bindMiddleware, sagaMiddleware } from './handlerStore'
export interface SagaStore extends Store<AppState> {
  [x: string]: any
  sagaTask?: Task | undefined
}

export const store = createStore(
  rootReducer,
  loadState(),
  bindMiddleware([sagaMiddleware])
)

export const stores: MakeStore<any> = () => {
  const _store: any = store
  _store.saga = sagaMiddleware.run(rootSaga)
  return _store as any
}

export const wrapper = createWrapper<SagaStore>(stores)
