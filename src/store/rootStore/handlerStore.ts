import { applyMiddleware, Middleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import createSagaMiddleware from 'redux-saga'

export const sagaMiddleware = createSagaMiddleware()

export const bindMiddleware = (middleware: Middleware[]) => {
  return composeWithDevTools(applyMiddleware(...middleware))
}
