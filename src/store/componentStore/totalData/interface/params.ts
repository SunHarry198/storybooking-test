export namespace TotalDataParams {
  export namespace FilterCheckData {
    export interface Response {
      data: any[]
    }
  }
  export namespace UploadImage {
    export interface Request {
      data: any
    }

    export interface response {
      url: any
    }
  }

  export namespace BookingInfo {
    export interface Request {
      idBooking?: string
      transactionId?: string
      smsCode?: any
    }
    export interface Response {
      data: any
    }
  }
}
