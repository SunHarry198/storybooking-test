import { TotalDataTypes, TotalDataParams } from '@interface'
export type TotalDataActions =
  | FilterCheckDataAction
  | UploadImageAction
  | BookingInfo

// GET PATIENT BY PHONE
export type FilterCheckDataAction = Response

export interface Response {
  type: TotalDataTypes.FilterCheckData.Response
  payload: TotalDataParams.FilterCheckData.Response
}
export type UploadImageAction = RequsetUploadImage | responseUploadImage

export interface RequsetUploadImage {
  type: TotalDataTypes.UploadImage.request
  payload: TotalDataParams.UploadImage.Request
}
export interface responseUploadImage {
  type: TotalDataTypes.UploadImage.response
  payload: TotalDataParams.UploadImage.response
}

// LẤY THÔNG TIN BOOKING INFO

export type BookingInfo = RequestBookingInfo | ResponseBookingInfo

export interface RequestBookingInfo {
  type: TotalDataTypes.BookingInfo.Request
  payload: TotalDataParams.BookingInfo.Request
}

export interface ResponseBookingInfo {
  type: TotalDataTypes.BookingInfo.Response
  payload: TotalDataParams.BookingInfo.Response
}
