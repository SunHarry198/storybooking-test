import { FilterParams, FilterTypes, FilterActions } from '@interface'
export const checkFilter = ({
  isValid
}: FilterParams.Filter.Check): FilterActions => {
  return {
    type: FilterTypes.Filter.Check,
    payload: { isValid }
  }
}

export const getNextStep = ({
  id,
  insurance,
  plattform,
  typeNext,
  urlImg,
  ngayHenTaiKham,
  thoiDiemBHYT,
  partnerid
}: FilterParams.Filter.GetNextStep): FilterActions => {
  return {
    type: FilterTypes.Filter.GetNextStep,
    payload: {
      id,
      insurance,
      plattform,
      typeNext,
      urlImg,
      ngayHenTaiKham,
      thoiDiemBHYT,
      partnerid
    }
  }
}
