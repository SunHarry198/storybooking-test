import { FilterTypes, FilterParams } from '@interface'
export type FilterActions = FilterAction

// GET PATIENT BY PHONE
export type FilterAction = CheckFilter | GetNextStep

export interface CheckFilter {
  type: FilterTypes.Filter.Check
  payload: FilterParams.Filter.Check
}

export interface GetNextStep {
  type: FilterTypes.Filter.GetNextStep
  payload: FilterParams.Filter.GetNextStep
}
