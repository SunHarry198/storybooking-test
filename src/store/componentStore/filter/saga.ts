import actionStore from '@actionStore'
import { FilterTypes } from '@interface'
import { client } from '@config/medproSDK'
import { all, fork, put, takeLatest } from 'redux-saga/effects'
import * as DTO from './interface/action'
import { AxiosResponse } from 'axios'
import { openToast } from '@lib/support/notification'
import { get } from 'lodash'
import { routerByPartnerId } from '@lib/func'

function* filter({ payload }: DTO.GetNextStep) {
  try {
    const plattform = 'app'
    const platform = 'pc'

    const partnerid = payload.partnerid
    let postNext: any

    if (payload.typeNext === 'vacxin') {
      postNext = {
        sessionId: payload.id,
        sessionData: {
          id: payload?.id,
          insurance: payload?.insurance,
          plattform: payload?.plattform
        }
      }
    } else {
      const objCheckFilter = {
        treeId: '',
        stepId: 'CheckBHYT',
        serviceId: '',
        template: 'INSURANCE',
        hasInsurance: true,
        isValid: true
      }

      const response1: AxiosResponse = yield client.checkFilter(
        objCheckFilter,
        {
          partnerid,
          platform
        }
      )
      const { data } = response1
      // ----------> app
      postNext = {
        sessionData: {
          hasInsuranceCode: true,
          id: payload?.id,
          insurance: payload?.insurance,
          plattform: payload?.plattform,
          urlImg: payload.urlImg,
          ngayHenTaiKham: payload.ngayHenTaiKham,
          thoiDiemBHYT: payload.thoiDiemBHYT
          // insuranceCode: patient.selectedPatient.insuranceCode,
          // hasInsuranceCode:
          //   total.service.serviceType === total.service.serviceTypeDefault
        },
        sessionId: plattform === 'app' ? payload.id : data.id || ''
      }
    }

    const response: AxiosResponse = yield client.getNextStep(postNext, {
      partnerid,
      platform,
      appid: partnerid,
      token:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IiIsInN1YiI6MCwidXNlck1vbmdvSWQiOiI2Mjk4NjEzYTdkZjUwYTAwMTkzMWNhNWYiLCJpYXQiOjE2NTg0ODQ1MjYsImV4cCI6NDgxNDI0NDUyNn0.JgbWfdT5GW33ta97EIJ_OiL1KQ30xyVqSeSUVaXXMYw'
    })
    if (response) {
      const { origin, pathname } = window.location
      yield put(actionStore.responseFilterCheckData({ data: response.data.id }))
      if (plattform === 'app') {
        // app thì đổi url
        if (!pathname.includes('confirm')) {
          const index = pathname.indexOf('-app')
          const webUrl = `${pathname.slice(
            0,
            index + 4
          )}-confirm${pathname.slice(index + 4)}`
          const url = `${origin}${webUrl}`
          window.location.href = url
          window.location.assign(webUrl)
        }
      } else {
        if (plattform === 'desktop') {
          const route = routerByPartnerId('/xac-nhan-thong-tin', partnerid)
          window.location.assign(route)
        }
      }
    }
  } catch (error) {
    const err = get(error, 'response.data', '')
    openToast({
      type: 'error',
      message: err?.message || '',
      description: ''
    })
  }
}
function* watcher_filter() {
  yield takeLatest(FilterTypes.Filter.GetNextStep, filter)
}

const filterGuide = function* root() {
  yield all([fork(watcher_filter)])
}
export default filterGuide
