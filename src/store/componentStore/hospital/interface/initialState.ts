export interface HospitalState {
  listPayment: any
  selectedPaymentFee: any
  passSchedules: boolean
  paymentFee: {
    totalFee: number
    subTotal: number
    grandTotal: number
  }
}
