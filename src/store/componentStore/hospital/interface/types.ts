export namespace HospitalTypes {
  export enum PaymentMethod {
    Request = 'PaymentMethod -> Request',
    Response = 'PaymentMethod -> Response',
    Reset = 'PaymentMethod -> Reset',
    Select = 'PaymentMethod -> Select'
  }
}
