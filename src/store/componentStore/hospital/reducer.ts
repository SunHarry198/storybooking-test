import { HospitalActions, HospitalState, HospitalTypes } from '@interface'
import { HYDRATE } from 'next-redux-wrapper'

const init: HospitalState = {
  listPayment: {},
  passSchedules: false,
  selectedPaymentFee: {},
  paymentFee: {
    subTotal: -1,
    totalFee: 0,
    grandTotal: 0
  }
}

export default function TotalData(
  state = init,
  action:
    | HospitalActions
    | {
        type: typeof HYDRATE
        payload?: HospitalState
      }
): HospitalState {
  switch (action.type) {
    case HospitalTypes.PaymentMethod.Response:
      return {
        ...state,
        listPayment: action.payload.data
      }
    case HospitalTypes.PaymentMethod.Reset:
      return {
        ...state,
        listPayment: []
      }
    case HospitalTypes.PaymentMethod.Select:
      // eslint-disable-next-line no-case-declarations
      const { totalFee, subTotal, grandTotal } = action?.payload
        ?.selectPaymentFee as any

      return {
        ...state,
        selectedPaymentFee: action?.payload?.selectPaymentFee,
        passSchedules: true,
        paymentFee: {
          subTotal,
          totalFee,
          grandTotal
        }
      }
    default:
      return state
  }
}
