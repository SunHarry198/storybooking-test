import actionStore from '@actionStore'
import { HospitalTypes } from '@interface'
import { client } from '@config/medproSDK'
import { all, fork, put, takeLatest } from 'redux-saga/effects'
import * as DTO from './interface/action'
import { AxiosResponse } from 'axios'
import { clog } from '@lib/support/clog'
import { getError } from '@lib/utils/func'

function* getAllPaymentMethod({ payload }: DTO.RequsetPaymentMethod) {
  try {
    // const appState: AppState = yield select((state) => state)
    let header: any = {
      token:
        'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6IiIsInN1YiI6MCwidXNlck1vbmdvSWQiOiI2Mjk4NjEzYTdkZjUwYTAwMTkzMWNhNWYiLCJpYXQiOjE2NTg0ODQ1MjYsImV4cCI6NDgxNDI0NDUyNn0.JgbWfdT5GW33ta97EIJ_OiL1KQ30xyVqSeSUVaXXMYw',
      appid: 'umcmono'
    }
    if (payload.partnerId) {
      header = { ...header, partnerid: payload.partnerId }
    }
    // const schedule = appState.hospital.schedule
    const hospital = {
      multiSchedule: [150000, 16000],
      schedule: 170000,
      treeId: 'DATE',
      SelectPayHosFee: {
        amount: 150000
      },
      isRepayment: false,
      paymentInfo: {
        id: ''
      }
    }

    let postData: any = {}

    switch (hospital.treeId) {
      case 'FEE':
        postData = {
          price: Number(hospital?.SelectPayHosFee?.amount),
          groupId: 2
        }
        break

      case 'DATE':
      default:
        postData = {
          bookingId: hospital.isRepayment ? hospital.paymentInfo?.id : '',
          patientId: '3b6ff794da2543b89b8ddaa6431a1d7b',
          price: 150000,
          groupId: 1,
          treeId: 'DATE',
          serviceId: 'umc_service-150000',
          subjectId: 'umc_P(',
          doctorId: 'umc_801',
          bookingDate: '2022-08-17T07:52:52.000Z'
        }
        break
    }
    const response: AxiosResponse = yield client.getAllPaymentMethod(
      postData,
      header
    )

    yield put(actionStore.responsePaymentMethod({ data: response }))

    // if (size(response.data) === 1) {
    //   const item = response.data[0]
    //   const paymentTypes = item.paymentTypes[0]
    //   paymentTypes.keyCollapse = 0
    //   paymentTypes.methodId = item.methodId
    //   paymentTypes.agreement = item.agreement

    //   yield put(
    //     actionStore.selectPaymentFee({ selectPaymentFee: paymentTypes })
    //   )
    // }
  } catch (error) {
    yield put(actionStore.responsePaymentMethod({ data: [getError(error)] }))
    clog({ name: 'getAllPaymentMethod', child: error, type: 'error' })
  }
}
function* watcher_getPayment() {
  yield takeLatest(HospitalTypes.PaymentMethod.Request, getAllPaymentMethod)
}
const hospitalSaga = function* root() {
  yield all([fork(watcher_getPayment)])
}
export default hospitalSaga
