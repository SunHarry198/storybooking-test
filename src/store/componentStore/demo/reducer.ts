import { DemoActions } from './interface/action'
import { DemoState } from './interface/initialState'
const initState: DemoState = {
  data: ''
}

export const supportReducer = (
  state = initState,
  action: DemoActions | any
): DemoState => {
  switch (action.type) {
    default:
      return state
  }
}
