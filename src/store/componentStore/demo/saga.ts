import { all } from 'redux-saga/effects'

const demoSagas = function* root() {
  yield all([])
}

export { demoSagas }
