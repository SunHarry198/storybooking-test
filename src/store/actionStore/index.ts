/* eslint-disable import/no-anonymous-default-export */
import * as filter from '@componentStore/filter/action'
import * as totalData from '@componentStore/totalData/action'
import * as hospital from '@componentStore/hospital/action'

export default {
  ...filter,
  ...totalData,
  ...hospital
}
