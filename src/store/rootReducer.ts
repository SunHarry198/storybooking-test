import TotalData from '@componentStore/totalData/reducer'
import Hospital from '@componentStore/hospital/reducer'
import { AppState } from '@store/interface/appState'
import { HYDRATE } from 'next-redux-wrapper'
import { Reducer } from 'react'
import { AnyAction, combineReducers } from 'redux'

const reducers: any = {
  totalData: TotalData,
  hospital: Hospital
}

export const combinedReducers = combineReducers(reducers)
export const rootReducer: Reducer<AppState, AnyAction> = (state, action) => {
  switch (action.type) {
    case HYDRATE:
      return {
        ...state,
        ...action.payload
      }

    default:
      return combinedReducers(state, action)
  }
}
