declare module './*.module.less' {
    const resource: { [key: string]: string };
    export = resource;
}
declare module '/node_modules/@medpro/booking-libs'
declare module 'react-barcode'
declare module 'node-rsa'
declare module 'isomorphic-unfetch'

declare module 'classnames'
declare module 'react-messenger-customer-chat'
