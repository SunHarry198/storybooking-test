import { NextPage } from 'next'
import { ComponentType } from 'react'

export type Page<P = any> = NextPage<P> & {
  layout?: ComponentType<any, any>
  requireAuth?: boolean
  hostname?: string
  [extraProps: string]: any
}
