import { urlBE, urlLogin, urlMCO, urlSupportCSKH } from '@init/init'

export const API_MCO = urlMCO.TESTING
export const API_LOGIN = urlLogin.TESTING
export const API_BE = urlBE.TESTING
export const URL_SUPPORT_CSKH = urlSupportCSKH.TESTING
