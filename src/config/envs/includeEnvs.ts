import * as production from './production'
import * as testing from './testing'
import * as develop from './develop'

export type ENVObj = {
  develop: typeof develop
  testing: typeof testing
  production: typeof production
}

const exportedObject: ENVObj = {
  develop,
  testing,
  production
}

export default exportedObject
