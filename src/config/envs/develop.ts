import { urlBE, urlLogin, urlSupportCSKH } from '@init/init'

export const API_LOGIN = urlLogin.TESTING

export const API_BE = urlBE.TESTING
export const URL_SUPPORT_CSKH = urlSupportCSKH.DEVELOP
