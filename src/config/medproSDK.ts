import { Client } from 'medpro-sdk'
import { currentEnv } from './envs'

export const client = new Client({
  apiRoot: currentEnv.API_BE,
  appid: 'medpro',
  platform: 'pc'
})

export const server = new Client({
  apiRoot: currentEnv.API_BE,
  appid: 'medpro',
  platform: 'pc'
})
