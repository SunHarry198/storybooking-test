import { initializeApp, getApps } from 'firebase/app'
import { getAuth } from 'firebase/auth'

const firebaseConfig = {
  apiKey: 'AIzaSyAlhlyDwJvLyMscRfrRR_QdKjH_FmneiBY',
  authDomain: 'plantszing.firebaseapp.com',
  projectId: 'plantszing',
  storageBucket: 'plantszing.appspot.com',
  messagingSenderId: '1092630461067',
  appId: '1:1092630461067:web:1d88cdc4c99415b8beb28e',
  measurementId: 'G-8D7VXQ07KG'
}

if (!getApps().length) {
  initializeApp(firebaseConfig)
}

export const auth = getAuth()

export default firebaseConfig

export const GA_TRACKING_ID = 'UA-222796648-1'
