import { NextPage } from 'next'
import { ComponentType } from 'react'

export type Page<P = any> = NextPage<P> & {
  listHospital?: any[]
  layout?: ComponentType<any, any>
  requireAuth?: boolean
  appId?: string
  hostname?: string
  isCs?:boolean,
  [extraProps: string]: any
}
