import { server } from '@config/medproSDK'

export const getContentPages = async (pathName: any, appId: any) => {
  try {
    const response = await server.getHospitalContentPage(
      {
        key: pathName
      },
      {
        appid: appId
      }
    )
    return response.data
  } catch (error) {
    console.info('error_contentpages :>> ', error)
    return []
  }
}
