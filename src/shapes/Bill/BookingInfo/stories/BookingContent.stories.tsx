import { ComponentMeta, ComponentStory } from '@storybook/react'
import { mockBookingContentProps } from './BookingContent.mocks'
import BookingInfo from '../index'
import { BookingInfoProps } from './../../../../page-components/Booking/index'
import React from 'react'
// const removeTable = {
//   table: {
//     disable: true
//   }
// }
export default {
  title: 'Components/Bill',
  component: BookingInfo,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    backgroundColor: { control: 'color' }
  }
} as ComponentMeta<typeof BookingInfo>

const Template: ComponentStory<typeof BookingInfo> = (args) => (
  <BookingInfo {...args} />
)

export const BookingContent = Template.bind({})

BookingContent.args = {
  ...mockBookingContentProps.BookingContent
} as BookingInfoProps
