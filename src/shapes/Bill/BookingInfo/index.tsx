import { getDay, getTimeBooking } from './../../../lib/func'
import React from 'react'
import styles from './../styles.module.css'
import { BookingInfoProps } from './../../../page-components/Booking/index'

const BookingInfo: React.FC<BookingInfoProps> = ({ bookingInfo }: any) => {
  return (
    <>
      <div className={styles.modalContentBill}>
        <div className={styles.modalContentStt}>
          <div>Số thứ tự tiếp nhận</div>
          <div className={styles.modalContentSttNumber}>
            {bookingInfo?.sequenceNumber}
          </div>
        </div>
        <ul>
          {bookingInfo?.bookingCode && (
            <li>
              <span className={styles.label}>Mã phiếu:</span>
              <span className={styles.value}>{bookingInfo?.bookingCode}</span>
            </li>
          )}
          {bookingInfo?.service?.name && (
            <li>
              <div>Dịch vụ:</div>
              <article>{bookingInfo?.service?.name}</article>
            </li>
          )}
          {bookingInfo?.serviceType && (
            <li>
              <div>Hình thức khám:</div>
              <article>
                {bookingInfo?.serviceType === 'NO_INSURANCE_ONLY'
                  ? 'Không có BHYT'
                  : 'Có BHYT'}
              </article>
            </li>
          )}
          {(bookingInfo?.room?.name ||
            bookingInfo?.checkInRoom?.room ||
            bookingInfo?.checkInRoom?.bookingNote) && (
            <li>
              <div>Phòng khám:</div>
              <article>
                {bookingInfo?.room?.name
                  ? bookingInfo?.room?.name
                  : bookingInfo?.checkInRoom?.room ||
                    bookingInfo?.checkInRoom?.bookingNote}
              </article>
            </li>
          )}
          {bookingInfo?.subject?.name && (
            <li>
              <div>Chuyên khoa:</div>
              <article>{bookingInfo?.subject?.name}</article>
            </li>
          )}
          {bookingInfo?.doctor && (
            <li>
              <div>Bác sĩ:</div>
              <article>{bookingInfo?.doctor?.name}</article>
            </li>
          )}
          {bookingInfo?.status === 1 && (
            <>
              <li>
                <div>Ngày khám:</div>
                <article style={{ color: '#00c851' }}>
                  {bookingInfo?.date && getDay(bookingInfo?.date)}
                </article>
              </li>
              <li>
                <div>Giờ khám dự kiến:</div>
                <article style={{ color: '#00c851' }}>
                  {(bookingInfo?.bookingExpiredData?.bookingDate &&
                    getTimeBooking(
                      bookingInfo?.bookingExpiredData?.bookingDate
                    )) ||
                    bookingInfo?.extraInfo?.booking?.bv_booking_time}
                </article>
              </li>
            </>
          )}
          <li>
            <div>Phí khám:</div>
            <article>{bookingInfo?.service?.priceText}</article>
          </li>
        </ul>
      </div>
    </>
  )
}

export default BookingInfo
