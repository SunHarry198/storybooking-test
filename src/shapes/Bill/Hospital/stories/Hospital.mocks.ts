import { HospitalProps } from '../index'

const Hospital: HospitalProps = {
  backgroundColor: '#f0f2f5',
  item: {
    bookingCode: '12312A313'
  }
}

export const mockHospitalProps = {
  Hospital
}
