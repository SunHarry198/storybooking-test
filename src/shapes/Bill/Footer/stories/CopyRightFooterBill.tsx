import { ComponentMeta, ComponentStory } from '@storybook/react'
import { mockBookingContentProps } from './CopyRightFooterBill.mocks'
import CopyRightFooterBill, { CopyRightFooterBillProps } from '../index'
import React from 'react'
// const removeTable = {
//   table: {
//     disable: true
//   }
// }
export default {
  title: 'Components/Bill',
  component: CopyRightFooterBill,
  // More on argTypes: https://storybook.js.org/docs/react/api/argtypes
  argTypes: {
    backgroundColor: { control: 'color' }
  }
} as ComponentMeta<typeof CopyRightFooterBill>

const Template: ComponentStory<typeof CopyRightFooterBill> = (args) => (
  <CopyRightFooterBill {...args} />
)

export const CopyRightFooterBillContent = Template.bind({})

CopyRightFooterBillContent.args = {
  ...mockBookingContentProps.CopyRightFooterBill
} as CopyRightFooterBillProps
