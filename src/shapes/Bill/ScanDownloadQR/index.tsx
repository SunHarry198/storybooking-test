import Button from '@components/atoms/Button'
import Link from 'next/link'
import React from 'react'
import styles from '../styles.module.css'
import QRCode from 'qrcode.react'

const ScanDownloadQR = () => {
  return (
    <Link href=''>
      <a>
        <div className={styles.modalQRCode}>
          <div className={styles.modalQRCodeTitle}>
            <h3>Chúc mừng đặt khám thành công</h3>
            <div>
              Quý khách vui lòng cài đặt ứng dụng để xem chi tiết hướng dẫn và
              quản lý hồ sơ khám bệnh.
            </div>
          </div>

          <div className={styles.modalQRCodeWrapper}>
            <div className={styles.modalQRCodeImage}>
              <QRCode
                fgColor='#000000'
                size={80}
                value='https://qrco.de/bcQbe9'
              />
            </div>
            <div className={styles.modalQRCodeDownload}>
              <h2>
                CHẠM ĐỂ KHÁM <br /> SCAN ĐỂ TẢI APP
              </h2>
              <Button>Tải ứng dụng</Button>
            </div>
          </div>
        </div>
      </a>
    </Link>
  )
}

export default ScanDownloadQR
