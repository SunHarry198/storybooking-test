import cx from 'classnames'
import styles from './styles.module.css'

const DefaultLayout = ({ children }: any) => {
  return (
    <>
      <section className={styles.layout}>
        <main className={cx(styles.content)}>{children}</main>
      </section>
    </>
  )
}
export default DefaultLayout
