import cx from 'classnames'
import { Title } from './interface'
import styles from './styles.module.scss'

const Titles = ({
  tag = 'h2',
  classNameTag,
  className,
  extra,
  label,
  children,
  underline,
  alignFlex = 'left'
}: Title) => {
  const Tag = `${tag}` as keyof JSX.IntrinsicElements
  return (
    <div className={cx(styles.titleCustom, className, styles[alignFlex])}>
      {children || (
        <Tag
          className={cx(
            styles.titleTag,
            classNameTag,
            underline && styles.underline
          )}
        >
          <span dangerouslySetInnerHTML={{ __html: label }} />
        </Tag>
      )}
      <span>{extra}</span>
    </div>
  )
}
export default Titles
