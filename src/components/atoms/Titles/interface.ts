import { ReactNode } from 'react'

export interface Title {
  tag?: keyof JSX.IntrinsicElements
  classNameTag?: any
  className?: any
  extra?: any
  label?: any
  children?: ReactNode
  alignFlex?: 'left' | 'center' | 'right' | 'space-between'
  underline?: boolean
}
