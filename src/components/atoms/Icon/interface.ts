export interface IconProps {
  name: string
  width?: number
  fill?: any
  stroke?: any
  height?: number
  children?: any
  size?: number
  className?: any
}
