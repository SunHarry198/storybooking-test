import { IconProps } from './interface'
import styles from './styles.module.scss'
import cx from 'classnames'

export const Svg = (props: IconProps) => {
  const {
    width = 24,
    fill = 'none',
    height,
    size,
    stroke,
    children,
    className
  } = props
  return (
    <span
      className={cx(styles.iconCustom, className)}
      style={{
        minHeight: size || height || width,
        width: size || width
      }}
    >
      <svg
        width={size || width}
        height={size || height || width}
        xmlns='http://www.w3.org/2000/svg'
        fill={fill}
        stroke={stroke || 'currentColor'}
      >
        {children}
      </svg>
    </span>
  )
}
