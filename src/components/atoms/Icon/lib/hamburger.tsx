import { IconProps } from '../interface'

export default function index(props: IconProps) {
  return (
    <svg {...props} viewBox='0 0 16 16'>
      <path d='m2.75 12.25h10.5m-10.5-4h10.5m-10.5-4h10.5' />
    </svg>
  )
}
