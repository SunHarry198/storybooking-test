import { IconProps } from '../interface'

export default function index(props: IconProps) {
  return (
    <svg viewBox='0 0 16 16' {...props}>
      <rect height='7.5' width='10.5' y='6.75' x='2.75' />
      <path d='m4.75 6.25s-1-4.5 3.25-4.5 3.25 4.5 3.25 4.5' />
    </svg>
  )
}
