import { IconProps } from '../interface'

export default function index(props: IconProps) {
  return (
    <svg {...props} viewBox='0 0 16 16'>
      <path />
      <rect
        xmlns='http://www.w3.org/2000/svg'
        height='10.5'
        width='12.5'
        y='2.75'
        x='1.75'
      />
      <circle xmlns='http://www.w3.org/2000/svg' cy='7.5' cx='8' r='2.25' />
      <path
        xmlns='http://www.w3.org/2000/svg'
        d='m4.75 12.75c0-1 .75-3 3.25-3s3.25 2 3.25 3'
      />
    </svg>
  )
}
