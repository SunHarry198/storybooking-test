export default function plus() {
  return (
    <svg viewBox='0 0 16 16'>
      <path d='m12.75 7.75h-10m5-5v10' />
    </svg>
  )
}
