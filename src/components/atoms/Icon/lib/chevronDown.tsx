import { IconProps } from '../interface'

export default function index(props: IconProps) {
  return (
    <svg {...props} viewBox='0 0 16 16'>
      <path d='m3.75 5.75 4.25 4.5 4.25-4.5' />
    </svg>
  )
}
