import { IconProps } from '../interface'

export default function index(props: IconProps) {
  return (
    <svg {...props} viewBox='0 0 16 16'>
      <path d='m12.25 3.75-4.5 4.25l4.5 4.25m-5-8.5-4.5 4.25 4.5 4.25' />
    </svg>
  )
}
