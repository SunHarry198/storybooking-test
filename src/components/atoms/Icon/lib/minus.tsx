export default function first() {
  return (
    <svg viewBox='0 0 16 16'>
      <path d='m13.25 7.75h-10.5' />
    </svg>
  )
}
