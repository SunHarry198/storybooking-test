import * as React from 'react'
import { IconProps } from './interface'
import * as Names from './names'
import { Svg } from './Svg'

export const Icon: React.FC<IconProps> = (props) => {
  const Detail: any = Names[props?.name]

  if (!Detail) {
    return null
  }
  return (
    <Svg {...props}>
      <Detail {...props} />
    </Svg>
  )
}
