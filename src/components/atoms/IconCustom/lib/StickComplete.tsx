export default function index() {
  return (
    <svg viewBox='0 0 16 16'>
      <path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M8 0C12.4182 0 16 3.58182 16 8C16 12.4182 12.4182 16 8 16C3.58182 16 0 12.4182 0 8C0 3.58182 3.58182 0 8 0Z'
        fill='#52C41A'
      />
      <path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M12.727 4.2997C13.091 4.69931 13.091 5.3472 12.727 5.7468L7.30324 11.7003C7.12841 11.8922 6.8913 12 6.64407 12C6.39683 12 6.15972 11.8922 5.9849 11.7003L3.27304 8.72355C2.90899 8.32394 2.90899 7.67606 3.27304 7.27645C3.63708 6.87684 4.22732 6.87684 4.59137 7.27645L6.64407 9.52964L11.4086 4.2997C11.7727 3.9001 12.3629 3.9001 12.727 4.2997Z'
        fill='white'
      />
    </svg>
  )
}
