export default function index() {
  return (
    <svg viewBox='0 0 18 18'>
      <path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M9 17C4.58182 17 0.999999 13.4182 0.999999 9C0.999998 4.58182 4.58182 1 9 1C13.4182 1 17 4.58182 17 9C17 13.4182 13.4182 17 9 17Z'
        fill='white'
        stroke='#00B5F1'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
      <path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M10 5C10 5.55239 9.55239 6 9 6C8.44761 6 8 5.55239 8 5C8 4.44761 8.44761 4 9 4C9.55239 4 10 4.44761 10 5Z'
        fill='#00B5F1'
      />
      <path
        fillRule='evenodd'
        clipRule='evenodd'
        d='M9 14C8.44772 14 8 13.6518 8 13.2222L8 7.77778C8 7.34822 8.44771 7 9 7C9.55228 7 10 7.34822 10 7.77778L10 13.2222C10 13.6518 9.55228 14 9 14Z'
        fill='#00B5F1'
      />
    </svg>
  )
}
