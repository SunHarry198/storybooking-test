import { callSign } from '@lib/helper/sign'
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'

export default function ProtectedPage({ children }: { children: JSX.Element }) {
  const [state, setState] = useState({ loading: false })

  // const user = useAppSelector((state) => state.user)
  const router = useRouter()

  useEffect(() => {
    // const token = getCookieNext('jwt')?.token || user?.info?.token
    const token = 'adasda'

    if (!token) {
      setState(() => ({ loading: true }))
      callSign({ router, key: 'in' })
    } else {
      setState(() => ({ loading: false }))
    }
  }, [router])

  return state.loading ? <div>loading</div> : children
}
