import { ButtonEvent } from '@interface'
import { Icon } from '@components/atoms/Icon'
import React from 'react'
import styles from './styles.module.scss'
import Button from '../Button'

export const Scroll = () => {
  const [state, setState] = React.useState({
    hidden: true
  })

  React.useEffect(() => {
    const handleDeviceType = () => {
      const scrollableHeight =
        document.documentElement.scrollHeight -
        document.documentElement.clientHeight
      const GOLDEN_RATIO = 0.2

      if (
        document.documentElement.scrollTop / scrollableHeight >
        GOLDEN_RATIO
      ) {
        // show button
        setState({
          hidden: false
        })
      } else {
        // hide button
        setState({
          hidden: true
        })
      }
    }

    window.addEventListener('scroll', handleDeviceType)

    return () => {
      window.addEventListener('scroll', handleDeviceType)
    }
  }, [])

  function actionScroll(e: ButtonEvent) {
    const { name } = e.currentTarget
    if (name === 'chevronUp') {
      window.scrollTo({ top: 0, behavior: 'smooth' })
    } else {
      window.scrollTo({ top: document.body.scrollHeight, behavior: 'smooth' })
    }
  }
  return state.hidden ? null : (
    <div className={state.hidden ? styles.hidden : styles.scroll}>
      <div className={styles.groupBtn}>
        <Button
          type='primary'
          name='chevronUp'
          className={styles.btn}
          onClick={actionScroll}
        >
          <Icon name='chevronUp' />
        </Button>
        <Button
          type='primary'
          name='chevronDown'
          className={styles.btn}
          onClick={actionScroll}
        >
          <Icon name='chevronDown' />
        </Button>
      </div>
    </div>
  )
}
