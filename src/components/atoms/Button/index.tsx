import React from 'react'
import { Button as Btn } from 'antd'
import styles from './styles.module.css'
import cx from 'classnames'
import { ButtonType } from 'antd/lib/button'
import { ButtonHTMLType } from 'antd/lib/button/button'

export interface ButtonPropsCustom {
  className?: string
  type?: ButtonType
  htmlType?: ButtonHTMLType
  backgroundColor?: string
  borderColor?: string
  borderWidth?: string
  borderRadius?: string
  full?: boolean
  onClick?: () => void
  icon?: React.ReactNode
  children?: React.ReactNode
}

const Button: React.FC<ButtonPropsCustom> = ({
  children,
  className: classStyles,
  backgroundColor,
  borderColor,
  borderWidth,
  borderRadius,
  ...props
}) => {
  return (
    /// <Button icon={icon} >title</Button> => [icon title]
    /// <Button>icon</Button> => [icon]
    /// <Button>title</Button> => [title]
    <Btn
      type={props.type}
      className={cx(styles.btnAntd, classStyles)}
      style={
        props.full
          ? {
              width: '100%',
              backgroundColor,
              borderColor,
              borderWidth,
              borderRadius
            }
          : {
              width: 'auto',
              backgroundColor,
              borderColor,
              borderWidth,
              borderRadius
            }
      }
      {...props}
    >
      {props.icon ? (
        <>
          {props.icon} &nbsp; {children}
        </>
      ) : (
        children
      )}
    </Btn>
  )
}

export default Button
