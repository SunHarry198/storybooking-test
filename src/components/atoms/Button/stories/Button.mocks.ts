import { ButtonPropsCustom } from './../index'

const base: ButtonPropsCustom = {
  children: 'Button',
  full: false
}

export const mockAuthButtonProps = {
  base
}
