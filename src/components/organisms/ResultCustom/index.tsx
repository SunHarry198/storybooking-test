import { Button, Result, Space } from 'antd'
import Link from 'next/link'
import { useRouter } from 'next/router'
import React from 'react'

export interface ResultCustomIF {
  status?: any
  title?: any
  subTitle?: any
}

const ResultCustom = ({
  status = '404',
  title = 'Xin lỗi vì sự cố này !',
  subTitle
}: ResultCustomIF) => {
  const router = useRouter()
  const [confirmLoading, setConfirmLoading] = React.useState(false)

  const handleLoadingRefresh = () => {
    setConfirmLoading(true)
    setTimeout(() => {
      setConfirmLoading(false)
      router.reload()
    }, 2000)
  }
  return (
    <div>
      <Result
        style={{ margin: 'auto' }}
        status={status}
        title={title}
        subTitle={
          subTitle || (
            <p>
              Chúng tôi đang cố gắng khắc phục trong thời gian sớm nhất ! <br />{' '}
              Vui lòng quay lại sau !
            </p>
          )
        }
        extra={
          <Space>
            <Button
              type='primary'
              loading={confirmLoading}
              onClick={handleLoadingRefresh}
            >
              Làm mới
            </Button>
            <Button type='primary'>
              <Link href='/'>
                <a aria-label='goback home' style={{ color: 'white' }}>
                  {' '}
                  Về Trang chủ
                </a>
              </Link>
            </Button>
          </Space>
        }
      />
    </div>
  )
}
export default ResultCustom
