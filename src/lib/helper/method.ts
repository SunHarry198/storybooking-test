import { getAppInfo } from '@lib/utils/partner'
import { parseCookies } from './cookies'

export const getAppInfoCookies = async (ctx: any) => {
  const appInfo = parseCookies({ ctx, key: 'appInfo' })

  if (appInfo) {
    return {
      appId: appInfo.appId,
      partnerId: appInfo.partnerId
    }
  } else {
    const { partnerId, appId } = await getAppInfo({ ctx })
    return {
      partnerId,
      appId
    }
  }
}
