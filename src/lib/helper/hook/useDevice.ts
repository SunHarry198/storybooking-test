import { useRouter } from 'next/router'
import React, { useEffect, useState } from 'react'

const useDevice = () => {
  const router = useRouter()
  const [state, setState] = useState({ loading: false })

  const handleDeviceType = React.useCallback(() => {
    const userAgent = navigator?.userAgent || navigator?.vendor
    console.info('userAgent :>> ', userAgent)
  }, [])

  const handleUrl = React.useCallback((_url: URL, status?: any) => {
    setState({ loading: status })
  }, [])

  useEffect(() => {
    router.events.on('routeChangeStart', (url: URL) => handleUrl(url, true))
    router.events.on('routeChangeComplete', (url: URL) => handleUrl(url, false))
    router.events.on('routeChangeError', (url: URL) => handleUrl(url, false))

    window.addEventListener('resize', handleDeviceType)
    window.addEventListener('orientationchange', handleDeviceType)
    window.addEventListener('load', handleDeviceType)
    window.addEventListener('reload', handleDeviceType)

    return () => {
      router.events.off('routeChangeComplete', handleUrl)
      window.removeEventListener('resize', handleDeviceType)
      window.removeEventListener('orientationchange', handleDeviceType)
      window.removeEventListener('load', handleDeviceType)
      window.removeEventListener('reload', handleDeviceType)
    }
  }, [handleDeviceType, handleUrl, router.events])
  return { loading: state.loading }
}

export default useDevice
