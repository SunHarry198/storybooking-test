import { currentEnv } from '@config/envs'

export const callSign = ({ router }: any) => {
  const urlLogin = currentEnv.API_LOGIN
  router.push(urlLogin)
  window.localStorage.setItem('loginAt', router.asPath)
}
