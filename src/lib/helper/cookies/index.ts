import { keyMemory } from '@lib/utils/constants'
import * as cookie from 'cookie'
import { getCookie, removeCookies, setCookies } from 'cookies-next'
export const serializeCookies = ({
  ctx,
  key,
  value
}: {
  ctx: any
  key: any
  value: any
}) => {
  const _value = typeof value === 'object' ? JSON.stringify(value) : value
  ctx?.res && ctx?.res?.setHeader('Set-Cookie', cookie.serialize(key, _value))
}

export const parseCookies = ({ ctx, key }: { ctx: any; key: string }) => {
  const cook = ctx?.req ? ctx.req.headers.cookie || '' : document.cookie
  const dataCookie = cookie.parse(cook)

  if (dataCookie[key]) {
    if (JSON.parse(dataCookie[key])) {
      return JSON.parse(dataCookie[key])
    } else {
      return dataCookie[key]
    }
  }

  return null
}

export type KeyMemory = keyof typeof keyMemory

export const getKeyCookie = (key: KeyMemory) => {
  const _key = keyMemory[key]
  const getValue: any = getCookie(_key)
  if (getValue) {
    return JSON.parse(getValue)
  }

  return null
}

export const setKeyCookie = ({
  key,
  value
}: {
  key: KeyMemory
  value: any
}) => {
  setCookies(keyMemory[key], JSON.stringify(value))
}

export const removeKeyCookie = (key: KeyMemory) => {
  removeCookies(keyMemory[key])
}
