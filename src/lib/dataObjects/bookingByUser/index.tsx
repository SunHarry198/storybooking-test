import {
  BarcodeOutlined,
  ContactsOutlined,
  FileOutlined,
  PushpinOutlined,
  ScheduleOutlined,
  UserOutlined
} from '@ant-design/icons'
import { getDay } from '@lib/func'

export const bookingByUser = (item: any) => {
  return [
    {
      icon: <UserOutlined />,
      title: 'Bệnh nhân',
      value: `${item?.patient?.surname} ${item?.patient?.name}`
    },
    {
      icon: <FileOutlined />,
      title: 'Mã phiếu',
      value: item?.bookingCode
    },
    {
      icon: <ContactsOutlined />,
      title: 'Ngày đặt',
      value: getDay(item?.createdAt)
    },
    {
      icon: <PushpinOutlined />,
      title: 'Trạng thái',
      value: item?.description,
      style:
        item?.status === -2
          ? { background: '#b2b2b2' }
          : item?.status === 0
          ? { background: '#ec8686' }
          : { background: '#90ee90' }
    },
    {
      icon: <ScheduleOutlined />,
      title: 'Ngày khám',
      value: getDay(item?.date)
    },
    {
      icon: <BarcodeOutlined />,
      title: 'Mã thanh toán',
      value: item?.transactionId
    }
  ]
}
