import { message } from 'antd'
import styles from './styles.module.css'

export const funcCopy = async (txt: string) => {
  message.success('copy thành công ! \n' + txt)

  if (typeof navigator.clipboard === 'undefined') {
    const textArea = document.createElement('textarea')
    textArea.value = txt
    textArea.style.position = 'fixed'
    document.body.appendChild(textArea)
    textArea.focus()
    textArea.select()

    document.execCommand('copy')

    document.body.removeChild(textArea)
    return
  } else {
    navigator.clipboard?.writeText(txt)
  }
}

export const funcError = (element: string, errors: any) => {
  if (errors[element]) {
    return <p className={styles.txtError}>{errors[element]?.message}</p>
  }
}

export const upperFirst = (txt: string) => {
  return txt.charAt(0).toUpperCase() + txt.slice(1)
}

export const myLoader = ({ src, width, quality }: any): string => {
  return `${src}?w=${width}&q=${quality || 75}`
}

export const scrollById = ({ Id }: { Id: string }) => {
  setTimeout(() => {
    const element = document.getElementById(Id)
    if (element) element.scrollIntoView({ behavior: 'smooth' })
  }, 500)
}

export const handleNumber = (evt: any) => {
  const ch = String.fromCharCode(evt.which)
  if (!/[0-9]/.test(ch)) {
    evt.preventDefault()
  }
}

export const handleName = (evt: any) => {
  // const regex = new RegExp('^[a-zA-Z0-9]+$')
  const kyTuDacBiet = /[`~!@#$%^&*()_|+\-=?;:'",.<>\\{\\}\\[\]\\\\/]/g
  const key = String.fromCharCode(!evt.charCode ? evt.which : evt.charCode)
  if (kyTuDacBiet.test(key)) {
    evt.preventDefault()
    return false
  }
}
import moment from 'moment'
import format from './format'
import { ChangeUTF8 } from './utils/interface'
export const getDay = (date: any) => {
  const nlBEFormatter = new Intl.DateTimeFormat('nl-BE')
  return nlBEFormatter.format(new Date(date).getTime())
}

export const getTime = (date: any) => {
  const time = new Date(date)
  return moment(time).format('hh:mm')
}

export const getTimeBooking = (date: any) => {
  const arr = date.split(' ')
  return arr[0]
}

export const compareDay = (day: any) => {
  const date1 = new Date(day)
  const date2 = new Date()
  if (date1.getFullYear() > date2.getFullYear()) {
    return true
  } else if (date1.getMonth() > date2.getMonth()) {
    return true
  } else if (date1.getMonth() === date2.getMonth()) {
    if (date1.getDate() > date2.getDate()) {
      return true
    } else {
      return false
    }
  } else {
    return false
  }
}

export const searchFuc = (data: any, search: any) => {
  let result: any[] = []
  if (search !== '') {
    data.forEach((element: { surname: string; name: string; mobile: any }) => {
      const name = element.surname + ' ' + element.name

      if (
        format.name(element.mobile).indexOf(search) !== -1 ||
        format.name(name).indexOf(format.name(search)) !== -1
      ) {
        result = [...result, element]
      }
    })
    return result
  } else {
    return data
  }
}

export const routerByPartnerId = (route: any, partner: any) => {
  return `/${partner}${route}`
}

export const inputBirthDay = (evt: any) => {
  const ch = String.fromCharCode(evt.which)
  if (!/\d|\//.test(ch)) {
    evt.preventDefault()
  }
}
export const onChangeDate = ({ e, props, id }: ChangeUTF8) => {
  const { value } = e.target
  // const cleanString = value.replace(/[0-9]+/g, '') // nhận vào kiểu số nguyên
  const output = value?.substr(0, 10) // lấy 8 ký tự

  props.setValue(id, output)
}
