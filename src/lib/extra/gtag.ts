export const GA_TRACKING_ID = 'G-SNR61K7DP3'

// https://developers.google.com/analytics/devguides/collection/gtagjs/pages
export const pageView = (url: URL, id: string) => {
  window &&
    window.gtag('config', id, {
      page_path: url,
      page_title: document.title
    })
}
type GTagEvent = {
  action: string
  category: string
  label: string
  value: string
}

// https://developers.google.com/analytics/devguides/collection/gtagjs/events
export const event = ({ action, category, label, value }: GTagEvent) => {
  window.gtag('event', action, {
    event_category: category,
    event_label: label,
    value
  })
}
