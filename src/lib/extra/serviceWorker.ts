export const serviceWorker = () => {
  if ('serviceWorker' in navigator) {
    window.addEventListener('load', () => {
      navigator.serviceWorker.register('/sw.js').then(
        (registration) => {
          console.info(
            'Service Worker registration successful with scope: ',
            registration.scope
          )
        },
        (err) => {
          console.info('Service Worker registration failed: ', err)
        }
      )
    })
  }
}
