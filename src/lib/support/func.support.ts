export const arrAction = [
  'request',
  'response',
  'success',
  'update',
  'failure',
  'save',
  'edit',
  'select',
  'reset'
]

export const uppStringFirst = (txt: string) => {
  return txt.charAt(0).toUpperCase() + txt.slice(1)
}

export const handleInterfaceAction = ({
  nameClass = '',
  nameGroupAction = '',
  nameAction = '',
  tag
}: any) => {
  const step = tag
    .map((item: any) => {
      return `
    export interface ${uppStringFirst(nameAction) + uppStringFirst(item)} {
      type: ${uppStringFirst(nameClass)}Types.${uppStringFirst(
        nameAction
      )}.${item.toUpperCase()}
    }\n`
    })
    .join('')

  const func = tag
    .map((item: any) => {
      return `
    export const ${
      item + uppStringFirst(nameAction.replace('_', ''))
    } = (): ${uppStringFirst(nameClass)}Actions => {
      return {
        type: ${uppStringFirst(nameClass)}Types.${uppStringFirst(
        nameAction
      )}.${item.toUpperCase()}
      }
    }\n`
    })
    .join('')

  const nameType = tag
    .map((item: any) => {
      return uppStringFirst(nameAction) + uppStringFirst(item)
    })
    .join('\n | ')

  return `
// *Get interface action by tag handle:
    export type ${nameGroupAction}Action = ${nameType}
    ${step}
// *Get function by action handle:
    ${func}
  `
}

export const handleTypes = (name: string, arr: any[], nameEnum: string) => {
  const _name = name.concat(' -> ')
  const _enum = arr
    .map((item) => {
      return `\t${item.toUpperCase()} = "${_name + item.toUpperCase()}"`
    })
    .join(',\n')

  return `
   export enum ${uppStringFirst(nameEnum)} {
     ${_enum}
   }
  `
}

export const handleFunctionSaga = (name: string) => {
  return `
  function* ${name}() {
    try {
      const appState: AppState = yield select((state) => state)
      const header = headerSdk(appState)

      const response: AxiosResponse = yield call()
    } catch (error) {
      clog({ name: '${name}', child: error, type: 'error' })
    }
  }

  function* watcher_${name}() {
    yield takeLatest(NameClassTypes.NameAction.StatusAction, ${name})
  }


  [fork(watcher_${name})]
  `
}
