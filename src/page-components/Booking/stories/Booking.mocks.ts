import { BookingPageProps } from './../index'

const BookingInfo: BookingPageProps = {
  backgroundColor: '#f0f2f5',
  backgroundContent: '#ffffff',
  backgroundDescription: '#00c851',
  bookingInfo: {
    partner: {
      name: 'Bệnh viện Thiếu Niên Nhi Đồng Bạc Chì'
    },
    description: 'Đã thanh toán',
    bookingCode: '1A287KJF7123DAS',

    sequenceNumber: 46,
    service: {
      name: 'Khám dịch vụ',
      priceText: '150.000 VNĐ'
    },
    serviceType: 'NO_INSURANCE_ONLY',

    checkInRoom: {
      room: 'Phòng 60'
    },
    subject: {
      name: 'CHĂM SÓC GIẢM NHẸ'
    },
    doctor: {
      name: 'Nguyễn Đoàn Ngọc Mai'
    },
    status: 1,
    date: '2022-05-27T06:15:00.000Z',
    extraInfo: {
      booking: {
        bv_booking_time: '13:25'
      }
    },
    patient: {
      surname: 'Nguyen Van',
      name: 'Test',
      fullName: 'Nguyen Van Test',
      birthdate: '25/03/1997',
      birthyear: '1997',
      code: '123AA61238S'
    }
  }
}

export const mockBookingProps = {
  BookingInfo
}
