import BookingInfo from './../../shapes/Bill/BookingInfo'
import PatientInfo from './../../shapes/Bill/PatientInfo'
import React from 'react'
import styles from './styles.module.css'
import HospitalBill from './../../shapes/Bill/Hospital'
// import ScanDownloadQR from 'src/shapes/Bill/ScanDownloadQR'
import CopyRightFooterBill from './../..//shapes/Bill/Footer'
// import SendBookingBill from '@components/shape/Patient/SendBookingBill'
// const BookingBillInfo = ({ billInfo, smsCode }: any) => {

import cx from 'classnames'
export declare type BookingPageProps = {
  backgroundColor?: string
  backgroundContent?: string
  backgroundDescription?: string
} & BookingInfoProps
const BookingBillInfo: React.FC<BookingPageProps> = ({
  bookingInfo,
  backgroundColor,
  ...props
}) => {
  return (
    <div>
      <div className={styles.printBillPage} style={{ backgroundColor }}>
        <div className={styles.scanQRCode}>{/* <ScanDownloadQR /> */}</div>
        <div
          className={styles.printBill}
          style={{ backgroundColor: props.backgroundContent }}
        >
          <div className={styles.printBillTitle}>
            <article>PHIẾU KHÁM BỆNH</article>
            <span>{bookingInfo?.partner?.name}</span>
            <BorderCustom backgroundColor={backgroundColor} />
            <div className={styles.printBillCode}>
              <HospitalBill item={bookingInfo} />
              <div
                className={styles.success}
                style={{ backgroundColor: props.backgroundDescription }}
              >
                {bookingInfo?.description}
              </div>
            </div>
          </div>
          <div className={styles.hospitalInfo}>
            <BorderCustom backgroundColor={backgroundColor} />
            <BookingInfo bookingInfo={bookingInfo} />
          </div>
          <div className={styles.hospitalInfo}>
            <BorderCustom backgroundColor={backgroundColor} />
            <PatientInfo bookingInfo={bookingInfo} />
          </div>

          <div className={styles.printBillFooter}>
            <BorderCustom backgroundColor={backgroundColor} />
            <CopyRightFooterBill />
          </div>
        </div>
        {/* <div className={styles.sendBookingBill}>
        <SendBookingBill smsCode={smsCode} />
      </div> */}
      </div>
    </div>
  )
}

export default BookingBillInfo

export const BorderCustom = ({ backgroundColor }: any) => {
  return (
    <>
      <div
        className={cx(styles.borderCustom, styles.left)}
        style={{ backgroundColor }}
      />
      <div
        className={cx(styles.borderCustom, styles.right)}
        style={{ backgroundColor }}
      />
    </>
  )
}

export interface BookingInfoProps {
  bookingInfo?: {
    patient?: {
      surname?: string
      name?: string
      fullName?: string
      birthdate?: string
      birthyear?: string
      code?: string
    }
    partner?: {
      name?: string
    }
    description?: string
    bookingCode?: string
    sequenceNumber?: string | number
    service?: {
      name?: string
      priceText: string | number
    }
    serviceType?: 'NO_INSURANCE_ONLY' | 'INSURANCE'
    room?: {
      name?: string
    }
    checkInRoom?: {
      room?: string
      bookingNote?: string
    }
    subject?: {
      name?: string
    }
    doctor?: {
      name?: string
    }
    status?: number
    date?: string
    bookingExpiredData?: {
      bookingDate?: string
    }
    extraInfo?: {
      booking?: {
        bv_booking_time?: string
      }
    }
  }
}
