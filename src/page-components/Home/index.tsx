import React from 'react'

const Home = () => {
  return (
    <div style={{ textAlign: 'center', marginTop: 30 }}>
      <span style={{ marginRight: 30 }}>Home page</span>
    </div>
  )
}

export default Home
